package com.qf.bootredis;

import com.alibaba.fastjson.JSON;
import com.alipay.easysdk.factory.Factory;
import com.alipay.easysdk.kernel.util.ResponseChecker;
import com.alipay.easysdk.payment.facetoface.models.AlipayTradePrecreateResponse;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class AliPayTest {

    @Test
    void faceToFace() {
        try {
            // 商品名称-主题
            String subject = "iPhone14 pro max";
            // 交易号
            String tradeNo = "111111";
            // 支付总金额
            String totalAmount = "8999.00";
            AlipayTradePrecreateResponse response = Factory.Payment.FaceToFace().preCreate(subject, tradeNo, totalAmount);
            // 判断请求状态
            if (ResponseChecker.success(response)) {
                // 支付请求调用成功，还没有支付 支付宝会给用户返回一个支付二维码
                // 拿到二维码后 用户扫码 输入支付密码才能支付
                System.out.println("调用成功");
                System.out.println(JSON.toJSONString(response,2));
            } else {
                System.err.println("调用失败，原因：" + response.msg + "，" + response.subMsg);
            }
        } catch (Exception e) {
            System.err.println("调用遭遇异常，原因：" + e.getMessage());
            throw new RuntimeException(e.getMessage(), e);
        }
    }
}

