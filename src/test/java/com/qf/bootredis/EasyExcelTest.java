package com.qf.bootredis;

import com.alibaba.excel.EasyExcel;
import com.alibaba.fastjson.JSON;
import com.qf.bootredis.entity.StudentX;
import com.qf.bootredis.listener.ExcelListener;
import io.swagger.models.auth.In;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SpringBootTest
public class EasyExcelTest {


    @Test
    public void readExcelToObject(){
        //读取excel文件到集合中
        //read 读那个文件
        //head 读取到的数据封装程什么样的实体类
        //sheet 读取那个sheet页
        //doReadSync 同步读文件 会进行阻塞
        List<StudentX> studentX = EasyExcel.read("students.xlsx").head(StudentX.class).sheet().doReadSync();

        studentX.forEach(students -> {
            String string = JSON.toJSONString(students);
            System.out.println(string);
        });
    }

    @Test
    public void readExcelToMap(){
        List<Map<Integer,String>> list=EasyExcel.read("students.xlsx").sheet().doReadSync();
        list.forEach(System.out::println);
    }

    //使用监听器读取excel文件
    @Test
    void readExcelWithListener(){
        EasyExcel.read("students.xlsx",StudentX.class,new ExcelListener()).sheet().doRead();
    }

    //写excel文件
    @Test
    void writeExcel(){
        //data() 返回需要写入到excel的数据
        EasyExcel.write("学生信息表.xlsx",StudentX.class).sheet("学生信息").doWrite(data());
    }

    //准备学生数据
    private List<StudentX> data(){
        //stream流
        //1...100 list
        List<Integer> list= Stream.iterate(1,i ->i+1).limit(1000).collect(Collectors.toList());
        List<StudentX> studentX=list.stream().map(i ->new StudentX(i,"姓名"+i,"男",i,"1863810111"+i,new Date(),"河南郑州")).collect(Collectors.toList());
        return studentX;
    }
}
