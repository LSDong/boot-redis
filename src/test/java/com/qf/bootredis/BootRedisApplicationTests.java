package com.qf.bootredis;

import com.qf.bootredis.entity.User;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

import javax.annotation.Resource;
import java.util.Date;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

@SpringBootTest
class BootRedisApplicationTests {

    @Resource
    StringRedisTemplate stringRedisTemplate;


    @Resource
    RedisTemplate<String, Object> redisTemplate;

    @Test
    void contextLoads() {
        //使用StringRedisTemplate模板可以操作字串类型
        ValueOperations<String, String> ops = stringRedisTemplate.opsForValue();
        //ops.set("name","郭将军");


        //设置值 同时指定过期时间
        //TimeUnit 时间单位:DAYS/HOURS/MINUTES/SECONDS...
        //ops.set("age","18",20, TimeUnit.SECONDS);

        //Absent:缺席 不存在 key存在就什么都不干
        //Present:出席 存在 key如果存在就覆盖
        //ops.setIfAbsent("name","郭力凯");
        //ops.setIfPresent("name","郭力凯");

        ops.append("name","无所吊谓");


    }

    @Test
    void testJson(){
        ValueOperations<String, Object> ops = redisTemplate.opsForValue();
        User user = new User(1, "admin", new Date());
        ops.set("user",user);
        System.out.println("user保存成功");
    }

    @Test
    void getJson(){
        ValueOperations<String, Object> ops = redisTemplate.opsForValue();
        User user = (User) ops.get("user");
        System.out.println(user);
    }

    @Test
    void testCluster(){
        ValueOperations<String, String> ops = stringRedisTemplate.opsForValue();
        ops.set("name","哈哈");
    }
}
