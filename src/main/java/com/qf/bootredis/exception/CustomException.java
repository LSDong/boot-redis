package com.qf.bootredis.exception;

import java.io.Serializable;

public class CustomException extends Exception implements Serializable {
    private static final long serialVersionUID = 4950115565008501831L;

    public CustomException() {
    }

    public CustomException(String message) {
        super(message);
    }
}
