package com.qf.bootredis.exception;

import com.qf.bootredis.entity.R;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class MyControllerAdvice {

    @ExceptionHandler(CustomException.class)
    public R handler(CustomException e){
        return R.fail(e.getMessage());
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public R handler(IllegalArgumentException e){
        return R.fail(e.getMessage());
    }


    @ExceptionHandler(Exception.class)
    public R handler(Exception e){
        return R.fail("未知错误，请联系管理员");
    }
}
