package com.qf.bootredis.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.google.common.collect.Lists;
import com.qf.bootredis.entity.StudentX;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Slf4j
public class ExcelListener extends AnalysisEventListener<StudentX> {

    //批处理大小
    private static final Integer BATCH_SIZE = 5;
    private List<StudentX> list = Lists.newArrayListWithExpectedSize(BATCH_SIZE);
    @Override
    public void invoke(StudentX studentX, AnalysisContext analysisContext) {
        log.info("读取到了一条数据{}",studentX);
        list.add(studentX);
        if(list.size()>= BATCH_SIZE){
            saveDate();
            list=Lists.newArrayListWithExpectedSize(BATCH_SIZE);
        }
    }
     // excel整个文件读取完毕会执行doAfterAllAnalysed方法
     // 只执行一次
    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {
        saveDate();
    }



    //伪代码
    private void saveDate(){
        log.info("批量添加excel数据到数据库");
    }
}
