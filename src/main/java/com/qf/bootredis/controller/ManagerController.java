package com.qf.bootredis.controller;

import com.qf.bootredis.entity.Manager;
import com.qf.bootredis.entity.R;
import com.qf.bootredis.service.ManagerService;
import com.qf.bootredis.utils.IpUtil;
import com.qf.bootredis.utils.RedisUtil;
import com.tencentcloudapi.sms.v20210111.models.SendSmsRequest;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

@RestController
@RequestMapping("mng")
@RequiredArgsConstructor
@Api(tags = "用户管理")
public class ManagerController {

    final ManagerService service;

    final RedisUtil redisUtil;

    @PostMapping("register")
    @ApiOperation("用户注册")
    public R register(@RequestBody Manager manager){
        //后端进行参数校验
        Assert.hasLength(manager.getUsername(),"用户名不能为空");
        Assert.hasLength(manager.getPassword(),"密码不能为空");
        return service.register(manager);

    }

    @PostMapping("login")
    @ApiOperation("用户登录")
    public R login(String username, String password, String code, HttpServletRequest request,String mobile){
        //校验验证码
        String captcha = redisUtil.getString("mobile" + mobile);
        if(Objects.equals(captcha,code)){
            return service.login(username, password);
        }
        return R.fail("验证码错误");
    }
}
