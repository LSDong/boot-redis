package com.qf.bootredis.controller;

import cn.hutool.core.util.RandomUtil;
import com.qf.bootredis.entity.R;
import com.qf.bootredis.utils.RedisUtil;
import com.qf.bootredis.utils.SmsUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Random;

@RestController
@RequestMapping("sms")
@RequiredArgsConstructor
@Api(tags = "短信控制器")
public class SmsController {

    final SmsUtil smsUtil;
    final RedisUtil redisUtil;

    @PostMapping("send")
    @ApiOperation("发送短信")
    public R sendSms(String mobile){
        String random = RandomUtil.randomNumbers(6);
        if (smsUtil.sendSms(mobile,random)){
            if (StringUtils.isEmpty(mobile)){
                return R.fail("手机号不能为空");
            }else if (mobile.length()<11){
                return R.fail("手机号输入错误");
            }else {

                redisUtil.setString("mobile"+mobile,random);
                return R.ok("短信发送成功");
            }
        }
        return R.fail("短信发送失败");
    }
}
