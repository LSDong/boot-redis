package com.qf.bootredis.controller;

import com.alibaba.excel.EasyExcel;
import com.alibaba.fastjson.JSON;
import com.qf.bootredis.entity.R;
import com.qf.bootredis.entity.Student;
import com.qf.bootredis.entity.StudentX;
import com.qf.bootredis.utils.ExcelUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@RequestMapping("excel")
@RequiredArgsConstructor
public class ExcelController {

    /**
     * 文件下载（失败了会返回一个有部分数据的Excel）
     */
    @GetMapping("download")
    public void download(HttpServletResponse response) throws Exception {
        // 这里注意 有同学反应使用swagger 会导致各种问题，请直接用浏览器或者用postman
        response.setContentType("application/vnd.ms-excel");
        response.setCharacterEncoding("utf-8");
        // 这里URLEncoder.encode可以防止中文乱码 当然和easyexcel没有关系
        String fileName = URLEncoder.encode("学生信息", "UTF-8").replaceAll("\\+", "%20");
        response.setHeader("Content-disposition", "attachment;filename*=utf-8''" + fileName + ".xlsx");
        EasyExcel.write(response.getOutputStream(), StudentX.class).sheet("学生信息").doWrite(data());
    }

    @GetMapping("download2")
    public void download2(HttpServletResponse response) throws IOException {
        try {
            response.setContentType("application/vnd.ms-excel");
            response.setCharacterEncoding("utf-8");
            // 这里URLEncoder.encode可以防止中文乱码 当然和easyexcel没有关系
            String fileName = URLEncoder.encode("学生信息", "UTF-8").replaceAll("\\+", "%20");
            response.setHeader("Content-disposition", "attachment;filename*=utf-8''" + fileName + ".xlsx");
            EasyExcel.write(response.getOutputStream(), StudentX.class).autoCloseStream(Boolean.FALSE).sheet("学生信息").doWrite(data());
        } catch (Exception e) {
            // 重置response
            response.reset();
            response.setContentType("application/json");
            response.setCharacterEncoding("utf-8");
            R<String> r = R.fail("文件下载失败");
            response.getWriter().write(JSON.toJSONString(r));
        }
    }

    @PostMapping("upload")
    public R upload(MultipartFile file) throws IOException {
        List<StudentX> list = ExcelUtils.getExcelModelData(file.getInputStream(), StudentX.class);
        // 上传的数据是 调用业务层 把数据保存到数据库
        System.out.println(list);
        return R.ok("文件解析完成");
    }


    // 准备学生数据
    private List<StudentX> data() {
        // stream 流
        List<Integer> list = Stream.iterate(1, i -> i + 1).limit(100).collect(Collectors.toList());
        List<StudentX> students = list.stream().map(i -> new StudentX(i, "姓名" + i, "男", i, "1863810111" + i, new Date(), "河南郑州")).collect(Collectors.toList());
        return students;
    }


}
