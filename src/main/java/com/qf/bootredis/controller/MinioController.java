package com.qf.bootredis.controller;

import cn.hutool.core.io.FileTypeUtil;
import com.qf.bootredis.entity.R;
import com.qf.bootredis.service.MinioService;
import com.qf.bootredis.utils.FileTypeUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping("minio")
@Api(tags = "文件控制器")
public class MinioController {

    final MinioService minioService;


    @GetMapping("buckets")
    @ApiOperation("列出所有储存桶")
    public R listAllBuckets(){
        List<String> names = minioService.listBucketName();
        return R.ok(names);
    }

    @PostMapping("upload")
    @ApiOperation("上传文件")
    public R upload(MultipartFile multipartFile,String bucketName) throws IOException {
        //获取文件类型
        String fileType = FileTypeUtils.getFileType(multipartFile);
        //生产新的文件名称
        String uuid = UUID.randomUUID().toString().replace("-", "");
        //获取文件新名字 uuid+扩展名
        String fileName = uuid+"."+ FileTypeUtil.getType(multipartFile.getInputStream());
        //上传文件到minio
        String url = minioService.putObject(multipartFile, bucketName, fileName, fileType);
        return R.ok(url);

    }
}
