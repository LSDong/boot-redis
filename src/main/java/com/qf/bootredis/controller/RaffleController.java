package com.qf.bootredis.controller;

import com.qf.bootredis.entity.R;
import com.qf.bootredis.utils.RedisUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@RestController
@RequiredArgsConstructor
@RequestMapping("raffle")
@Api(tags = "抽奖程序")
public class RaffleController {
    final RedisUtil redisUtil;

    private static final String RAFFLE_KEY="choujiang";

    //添加抽奖人员
    @ApiOperation("添加抽奖人员")
    @PostMapping("add")
    public R addMembers(String name){
        String[] names = name.split(",");
        Long count = redisUtil.addMembers(RAFFLE_KEY, names);
        return R.ok(count);
    }

    //抽奖
    @ApiOperation("抽奖")
    @PostMapping("pop")
    public R pop(@RequestParam(value = "count",required = false,defaultValue = "1")Integer count){
        List<String> members = redisUtil.pop(RAFFLE_KEY, count);
        return R.ok(members);
    }

    //删除key
    @ApiOperation("删除key")
    @DeleteMapping("{key}")
    public R delete(@PathVariable String key){
        return R.ok(redisUtil.deleteKey(key));
    }


    //查询所有候选人
    @ApiOperation("查询所有候选人")
    @GetMapping("members")
    public R members(){
        Set<String> members = redisUtil.members(RAFFLE_KEY);
        return R.ok(members);
    }
}
