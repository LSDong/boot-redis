package com.qf.bootredis.controller;

import com.qf.bootredis.utils.IpUtil;
import com.qf.bootredis.utils.RedisUtil;
import com.wf.captcha.ArithmeticCaptcha;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
@RequiredArgsConstructor
@Api(tags = "验证码")
public class CaptchaController {

    final RedisUtil redisUtil;

    @GetMapping("captcha")
    @ApiOperation("获取验证码")
    public void captcha(HttpServletResponse response, HttpServletRequest request) throws IOException {
        ArithmeticCaptcha captcha = new ArithmeticCaptcha();
        String code = captcha.text();
        //把验证码放到redis中
        String ipAddr = IpUtil.getIpAddr(request);
        redisUtil.setString("code:"+ipAddr,code);
        //返回验证码图片
        response.setHeader("content-type","image/jpeg");
        captcha.out(response.getOutputStream());
    }
}
