package com.qf.bootredis.controller;


import com.qf.bootredis.entity.R;
import com.qf.bootredis.service.QRService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
@RequestMapping("qr")
@RequiredArgsConstructor
public class QrController {

    final QRService qrService;

    @GetMapping
    public void generaImage(String content, HttpServletResponse response) throws IOException {
        response.setHeader("content-type","image/png");
        qrService.generateToStream(content,response);
    }

    @GetMapping("base64")
    public R<String> generateImageToBase64(String content){
        String base64= qrService.generateBase64(content);
        return R.ok(base64);
    }
}
