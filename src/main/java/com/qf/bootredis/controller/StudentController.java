package com.qf.bootredis.controller;

import com.github.pagehelper.PageInfo;
import com.qf.bootredis.entity.R;
import com.qf.bootredis.entity.Student;
import com.qf.bootredis.service.StudentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("stu")
@RequiredArgsConstructor
@Api(tags = "学生管理")
public class StudentController {

    final StudentService service;

    @RequestMapping("list")
    @ApiOperation("查询所有学生")
    public R<PageInfo<Student>> listAll(@RequestParam(value = "page",required = false,defaultValue = "1")Integer page,
                                        @RequestParam(value = "size",required = false,defaultValue = "10")Integer size){
        PageInfo<Student> pageInfo = service.selectByPage(page, size);
        return R.ok(pageInfo);

    }

    @GetMapping("{id}")
    @ApiOperation("查询单个学生")
    public R<Student> selectById(@PathVariable Integer id){
        Student student = service.selectById(id);
        return R.ok(student);
    }

    @PostMapping("add")
    @ApiOperation("添加学生")
    public R<String> add(Student student){
        if (service.insert(student)){
            return R.ok("添加成功");
        }
        return R.fail("添加失败");
    }


    @PostMapping("update")
    @ApiOperation("修改学生信息")
    public R<String> update(Student student){
        if (service.update(student)) {
            return R.ok("修改成功");
        }
        return R.fail("修改失败");

    }

    @DeleteMapping("{id}")
    @ApiOperation("删除学生")
    public R<String> deleteById(@PathVariable Integer id){
        if (service.deleteById(id)) {
            return R.ok("删除成功");
        }
        return R.fail("删除失败");

    }
}
