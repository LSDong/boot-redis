package com.qf.bootredis.controller;

import com.alipay.easysdk.factory.Factory;
import com.alipay.easysdk.kernel.util.ResponseChecker;
import com.alipay.easysdk.payment.common.models.AlipayTradeQueryResponse;
import com.alipay.easysdk.payment.facetoface.models.AlipayTradePrecreateResponse;
import com.qf.bootredis.entity.R;
import com.qf.bootredis.service.QRService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.Objects;

@RestController
@RequestMapping("alipay")
@Api(tags = "支付控制器")

@RequiredArgsConstructor
public class AlipayController {

    final QRService qrService;
    @PostMapping("pay")
    @ApiOperation("获取支付二维码")
    public void pay(String subject, String code, String totalMoney, HttpServletResponse resp) throws Exception {
        AlipayTradePrecreateResponse response = Factory.Payment.FaceToFace().preCreate(subject, code, totalMoney);
        if (ResponseChecker.success(response)){
            //请求支付成功，生成二维码
            if(Objects.equals(response.getCode(),"10000")){
                resp.setHeader("content-type","image/png");
                qrService.generateToStream(response.getQrCode(),resp);
            }
        }

    }


    @GetMapping("query")
    @ApiOperation("查询支付状态")
    public R<String> queryPayState(String code) throws Exception {
        AlipayTradeQueryResponse response = Factory.Payment.Common().query(code);
        String tradeStatus = response.getTradeStatus();
        if (StringUtils.isEmpty(tradeStatus)){
            return R.fail("交易不存在");
        }else if (Objects.equals(tradeStatus,"WAIT_BUYER_PAY")){
            return R.ok("等待买家付款");
        }else if (Objects.equals(tradeStatus,"TRADE_SUCCESS")){
            return R.ok("支付成功");
        }
        return R.fail("状态查询失败");
    }
}
