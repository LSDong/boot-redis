package com.qf.bootredis.utils;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.read.builder.ExcelReaderBuilder;
import com.alibaba.excel.read.builder.ExcelReaderSheetBuilder;
import lombok.extern.slf4j.Slf4j;

import java.io.InputStream;
import java.util.List;

@Slf4j
public class ExcelUtils {

    public static <T> List<T> getExcelModelData(final InputStream inputStream, Class<T> clazz) {
        if (null == inputStream) {
            throw new NullPointerException("the inputStream is null!");
        }
        ExcelReaderBuilder result = EasyExcel.read(inputStream, clazz, null);
        ExcelReaderSheetBuilder sheet1 = result.sheet();
        // 同步读取文件
        List<T> resultData = sheet1.doReadSync();
        return resultData;
    }
}
