package com.qf.bootredis.utils;

import io.swagger.models.auth.In;
import lombok.RequiredArgsConstructor;
import org.springframework.data.redis.core.*;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;


    @Component
    @RequiredArgsConstructor
    public class RedisUtil {

        final StringRedisTemplate stringRedisTemplate;

        final RedisTemplate<String, Object> redisTemplate;

        public void setString(String key, String value) {
            ValueOperations<String, String> ops = stringRedisTemplate.opsForValue();
            ops.set(key, value);
        }

        public void setString(String key, String value, long time) {
            ValueOperations<String, String> ops = stringRedisTemplate.opsForValue();
            ops.set(key, value, time, TimeUnit.SECONDS);
        }

        public String getString(String key) {
            ValueOperations<String, String> ops = stringRedisTemplate.opsForValue();
            return ops.get(key);
        }

        // 存对象类型
        public void set(String key, Object value) {
            ValueOperations<String, Object> ops = redisTemplate.opsForValue();
            ops.set(key, value);
        }

        public void set(String key, Object value, long time) {
            ValueOperations<String, Object> ops = redisTemplate.opsForValue();
            ops.set(key, value, time, TimeUnit.SECONDS);
        }

        public Object get(String key) {
            ValueOperations<String, Object> ops = redisTemplate.opsForValue();
            return ops.get(key);
        }


        //添加抽奖人员
        public Long addMembers(String key,String[] members){
            SetOperations<String, String> ops = stringRedisTemplate.opsForSet();
            return ops.add(key,members);
        }

        //抽奖
        public List<String> pop(String key,Integer count){
            SetOperations<String, String> ops = stringRedisTemplate.opsForSet();
            return ops.pop(key,count);
        }

        //删除抽奖的key
        public Boolean deleteKey(String key){
            RedisOperations<String, String> ops = stringRedisTemplate.opsForValue().getOperations();
            return ops.delete(key);
        }

        //查看候选人
        public Set<String> members(String key){
            SetOperations<String, String> ops = stringRedisTemplate.opsForSet();
            return ops.members(key);
        }

    }


