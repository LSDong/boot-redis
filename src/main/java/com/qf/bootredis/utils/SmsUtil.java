package com.qf.bootredis.utils;

import com.qf.bootredis.config.SmsProperties;
import com.tencentcloudapi.common.Credential;
import com.tencentcloudapi.common.profile.ClientProfile;
import com.tencentcloudapi.common.profile.HttpProfile;

import com.tencentcloudapi.sms.v20210111.SmsClient;
import com.tencentcloudapi.sms.v20210111.models.SendSmsResponse;
import com.tencentcloudapi.sms.v20210111.models.SendSmsRequest;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class SmsUtil {

    @Resource
    SmsProperties properties;

    public Boolean sendSms(String mobile,String code){
        try{

            Credential cred = new Credential(properties.getSecretId(),properties.getSecretKey());

            //实例化一个Http选项，可选，没有特殊需求可以跳过
            HttpProfile httpProfile = new HttpProfile();
            httpProfile.setReqMethod("POST");
            httpProfile.setEndpoint(properties.getEndpoint());

            ClientProfile clientProfile = new ClientProfile();
            clientProfile.setSignMethod("HmacSHA256");
            clientProfile.setHttpProfile(httpProfile);

            SmsClient client = new SmsClient(cred, properties.getRegion(), clientProfile);
            SendSmsRequest req = new SendSmsRequest();
            //签名ID
            req.setSmsSdkAppId(properties.getAppId());
            //签名内容
            req.setSignName(properties.getSignName());
            //模板ID
            req.setTemplateId(properties.getTemplateId());

            String[] templateParamSet = {code};
            req.setTemplateParamSet(templateParamSet);
            /* 下发手机号码，采用 E.164 标准，+[国家或地区码][手机号]
             * 示例如：+8613711112222， 其中前面有一个+号 ，86为国家码，13711112222为手机号，最多不要超过200个手机号 */
            String[] phoneNumberSet={mobile};
            req.setPhoneNumberSet(phoneNumberSet);

            //发送短信
            SendSmsResponse res = client.SendSms(req);
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }
}
