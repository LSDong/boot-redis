package com.qf.bootredis.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class User implements Serializable {
    private static final long serialVersionUID = 7622471261350871249L;

    private Integer id;

    private String name;


    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date birthday;


}
