package com.qf.bootredis.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.DateTimeFormat;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import com.alibaba.fastjson.annotation.JSONField;
import io.swagger.models.auth.In;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
//写excel文件时 excel文件的列和单元格的宽高样式
@ColumnWidth(20)
@HeadRowHeight(15)
@ContentRowHeight(10)
public class StudentX {
    @ExcelProperty("序号")
    @ColumnWidth(10)
    private Integer id;

    @ExcelProperty("姓名")
    private String name;

    @ExcelProperty("性别")
    @ColumnWidth(10)
    private String sex;

    @ExcelProperty("年龄")
    private Integer age;

    @ExcelProperty("手机号")
    private String mobile;

    @ExcelProperty("毕业时间")
    @DateTimeFormat("yyyy/MM/dd")
    @JSONField(format = "yyyy/MM/dd")
    private Date graduationDate;

    @ExcelProperty("家庭地址")
    @ColumnWidth(50)
    private String address;

}
