package com.qf.bootredis.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * (Student)实体类
 *
 * @author mudong
 * @since 2023-05-31 19:48:57
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Student implements Serializable {
    private static final long serialVersionUID = 653528890574145674L;
    /**
     * ID
     */
    private Integer id;
    /**
     * 姓名
     */
    private String name;
    /**
     * 班级
     */
    private String grade;
    /**
     * 性别
     */
    private String sex;
    /**
     * 年龄
     */
    private Integer age;
    /**
     * 学历
     */
    private String degree;
    /**
     * 分数
     */
    private Integer score;
    /**
     * 简介
     */
    private String profile;
    /**
     * 出勤情况
     */
    private Integer attendance;
    /**
     * 面试
     */
    private String interview;
    /**
     * 简历
     */
    private String resume;

}

