package com.qf.bootredis.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * (Manager)实体类
 *
 * @author mudong
 * @since 2023-05-31 11:48:31
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Manager implements Serializable {
    private static final long serialVersionUID = -80355699176092141L;

    private Integer id;

    private String username;

    private String password;

    private String email;

}

