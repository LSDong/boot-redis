package com.qf.bootredis.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "tencent")
public class SmsProperties {

     private String endpoint;
     private String secretId;
     private String secretKey;
     private String appId;
     private String region;
     private String signName;
     private String templateId;
}
