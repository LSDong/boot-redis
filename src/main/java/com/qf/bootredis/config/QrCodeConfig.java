package com.qf.bootredis.config;


import cn.hutool.extra.qrcode.QrConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.awt.*;

@Configuration
public class QrCodeConfig {
    @Bean
    public QrConfig qrConfig(){
        QrConfig config = new QrConfig();
        //设置二维码前景色
        config.setForeColor(Color.BLACK);
        //设置二维码的背景色
        config.setBackColor(Color.WHITE);
        return config;
    }

}
