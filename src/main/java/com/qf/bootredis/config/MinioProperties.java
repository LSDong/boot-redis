package com.qf.bootredis.config;

import io.minio.MinioClient;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "minio")
public class MinioProperties {

    /**
     * 端点
     */
    private String endpoint;
    /**
     * 端口
     */
    private Integer port;
    /**
     * 访问密钥
     */
    private String accessKey;
    /**
     * 秘密密钥
     */
    private String secretKey;
    /**
     * 安全
     */
    private Boolean secure;
    /**
     * bucket存储桶名称
     */
    private String bucketName;
    /**
     * 图像大小
     */
    private Integer imageSize;
    /**
     * 文件大小
     */
    private Integer fileSize;

    /**
     * 官网给出的构造方法，我只是去爬了一下官网
     * 此类是 客户端进行操作的类
     */
    @Bean
    public MinioClient minioClient(){
        return MinioClient.builder()
                .credentials(accessKey,secretKey)
                .endpoint(endpoint,port,secure)
                .build();
    }

}

