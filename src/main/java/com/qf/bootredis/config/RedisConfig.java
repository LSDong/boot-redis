package com.qf.bootredis.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;

@Configuration
public class RedisConfig {

    //到spring的IOC容器中注入一个修改后的RedisTemplate

    @Bean
    public RedisTemplate<String,Object> redisTemplate(RedisConnectionFactory factory){
        RedisTemplate<String,Object> template=new RedisTemplate<String,Object>();
        //关联redis连接工厂
        template.setConnectionFactory(factory);
        //修改redis的key喝value的序列化方案
        template.setKeySerializer(RedisSerializer.string());
        //value使用json进行序列化和反序列化
        template.setValueSerializer(RedisSerializer.json());

        return template;
    }
}
