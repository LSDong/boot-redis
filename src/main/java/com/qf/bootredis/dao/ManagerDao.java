package com.qf.bootredis.dao;

import com.qf.bootredis.entity.Manager;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ManagerDao {

    Manager selectByName(String username);

    int insert(Manager manager);
}
