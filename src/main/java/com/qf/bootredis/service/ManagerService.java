package com.qf.bootredis.service;

import com.qf.bootredis.entity.Manager;
import com.qf.bootredis.entity.R;

public interface ManagerService {

    R login(String username,String password);

    R register(Manager manager);
}
