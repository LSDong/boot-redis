package com.qf.bootredis.service.impl;

import cn.hutool.crypto.SecureUtil;
import com.qf.bootredis.dao.ManagerDao;
import com.qf.bootredis.entity.Manager;
import com.qf.bootredis.entity.R;
import com.qf.bootredis.service.ManagerService;
import com.qf.bootredis.utils.RedisUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;

@Service
@RequiredArgsConstructor
@Transactional(rollbackFor = Exception.class)
public class ManagerServiceImpl implements ManagerService {

    final ManagerDao managerDao;

    final RedisUtil redisUtil;
    @Override
    public R login(String username, String password) {
        Manager manager = managerDao.selectByName(username);

        if(Objects.nonNull(manager)){
            //比对密码
            String cryptoPwd = SecureUtil.sha256(password);
            if(Objects.equals(cryptoPwd,manager.getPassword())){
                //登陆成功后，把用户信息放到redis 中
                redisUtil.set("manager",manager);
                return R.ok(manager);
            }else {
                return R.fail("用户名或密码错误");
            }
        }
        return R.fail("用户名不存在");
    }

    @Override
    public R register(Manager manager) {
        //进行密码枷锁
        //获取用户设置的明文密码

        String password = manager.getPassword();

        //对称加密 AES 安全级别不是很高 加密的效率高
        //非对称加密 【RSA】 公钥和私钥 加密效率低
        String cryptoPwd = SecureUtil.sha256(password);
        manager.setPassword(cryptoPwd);
        if (managerDao.insert(manager)>0){
            return R.ok("新增用户成功");
        }
        return R.fail("新增用户失败");
    }
}
