package com.qf.bootredis.service.impl;

import cn.hutool.extra.qrcode.QrCodeUtil;
import cn.hutool.extra.qrcode.QrConfig;
import com.qf.bootredis.service.QRService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Service
@RequiredArgsConstructor
public class QRServiceImpl implements QRService {

    final QrConfig qrConfig;
    @Override
    public void generateToStream(String content, HttpServletResponse response) throws IOException {
        QrCodeUtil.generate(content,qrConfig,"png",response.getOutputStream());
    }

    @Override
    public String generateBase64(String content) {
        return QrCodeUtil.generateAsBase64(content,qrConfig,"png");
    }
}
