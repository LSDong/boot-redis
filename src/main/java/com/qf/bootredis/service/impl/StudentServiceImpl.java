package com.qf.bootredis.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.qf.bootredis.dao.StudentDao;
import com.qf.bootredis.entity.Student;
import com.qf.bootredis.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
@Transactional(rollbackFor = Exception.class)
public class StudentServiceImpl implements StudentService {

    final StudentDao studentDao;
    @Override
    public PageInfo<Student> selectByPage(Integer page, Integer size) {
        PageHelper.startPage(page,size);
        List<Student> list = studentDao.listAll();
        PageInfo<Student> info = new PageInfo<>(list);

        return info;
    }

    @Override
    public Student selectById(Integer id) {
        return studentDao.queryById(id);
    }

    @Override
    public Boolean insert(Student student) {
        return studentDao.insert(student)>0;
    }

    @Override
    public Boolean update(Student student) {
        return studentDao.update(student)>0;
    }

    @Override
    public Boolean deleteById(Integer id) {
        return studentDao.deleteById(id)>0;
    }
}
