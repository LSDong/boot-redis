package com.qf.bootredis.service;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public interface QRService {

    void generateToStream(String content, HttpServletResponse response)throws IOException;

    String generateBase64(String content);
}
