package com.qf.bootredis.service;

import com.github.pagehelper.PageInfo;
import com.qf.bootredis.entity.Student;

public interface StudentService {
    PageInfo<Student> selectByPage(Integer page, Integer size);

    Student selectById(Integer id);

    Boolean insert(Student student);

    Boolean update(Student student);

    Boolean deleteById(Integer id);
}
